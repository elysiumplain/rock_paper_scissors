from enum import Enum, auto


class Entity(Enum):
    ROCK = auto()
    PAPER = auto()
    SCISSORS = auto()
    SPOCK = auto()
    LIZARD = auto()
